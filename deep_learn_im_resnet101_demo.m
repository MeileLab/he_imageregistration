clear all;
close all;
format long

%% Load Pretrained Network
sprintf('Initiating network')
net = resnet101;
inputSize = net.Layers(1).InputSize;
%analyzeNetwork(net)
%% loading images for testing
imds = imageDatastore('FISH_nano','IncludeSubfolders',true,'LabelSource','foldernames');
[imdsTrain,imdsTest] = splitEachLabel(imds,0.7,'randomized');
%% Extract Image Features
augimdsTrain = augmentedImageDatastore(inputSize(1:2),imdsTrain);
augimdsTest = augmentedImageDatastore(inputSize(1:2),imdsTest);

% layer = 'pool5';

%feature dimention (mxn) is from the size of layer, e.g.
%res5b_relu has 7*7*512, gives the n = 7*7*512, where m is the number of
%images

%res3b_relu has 28*28*512, gives the n = 7*7*512, where m is the number of
%images
%{
featuresTrain = activations(net,augimdsTrain,layer1,'OutputAs','rows');
featuresTest = activations(net,augimdsTest,layer1,'OutputAs','rows');

%whos featuresTrain
%Train Classifier on Shallower Features
%layer = 'res3b_relu';
%featuresTrain = activations(net,augimdsTrain,layer);
%featuresTest = activations(net,augimdsTest,layer);
%whos featuresTrain
YTrain = imdsTrain.Labels;
YTest = imdsTest.Labels;
classifier = fitcecoc(featuresTrain,YTrain);
YPred = predict(classifier,featuresTest);
idx = [1 5 10 15];

figure
for i = 1:numel(idx)
    subplot(2,2,i)
    I = readimage(imdsTest,idx(i));
    label = YPred(idx(i));
    imshow(I)
    title(char(label))
end
accuracy = mean(YPred == YTest)
%}

%% uncropped image
movingu = imageDatastore('FISH_nano/fish','LabelSource','foldernames');
fixedu = imageDatastore('FISH_nano/nano','LabelSource','foldernames');


%% use feature points @layer1

moving0 = imageDatastore('FISH_nano/fishcrop','LabelSource','foldernames');
fixed0 = imageDatastore('FISH_nano/nanocrop','LabelSource','foldernames');
fullFileNamesm0 = vertcat(moving0.Files);
fullFileNamesf0 = vertcat(fixed0.Files);
%baseFileNameWithExt = [ baseFileNameNoExt, ext];

%{
mkdir('bwfish')
mkdir('bwnano')


%binarize use only first time
for i = 1:42
    close all;
    fram = 0.9;
    fraf = 0.85;
    fbwm = 250;
    fbwf = 250;    
    if i == 8
        fraf = 0.8;
    elseif i == 1
        fraf = 0.8;
    elseif i == 4
        fraf = 1.25;
    elseif i == 6
        fraf = 1.2;
        fram = 1;
        fbwf = 200;
    elseif i == 7
        fraf = 1;
        fram = 1.35;  
        fbwm = 2000;
    elseif i == 9 
        fbwm = 2000;
    elseif (i == 10)
        fraf = 1;
        fram = 1.2;  
        fbwm = 1000;fbwf = 750;
    elseif (i == 11) || (i == 12)
        fraf = 1.15;
        fram = 0.65;  
        %fbwf = 100;
        %fbwm = 100;
    elseif (i == 17)
        fraf = 1;
        fram = 0.65;    
    elseif (i == 19)
        fraf = 1.05;
        fram = 0.67;     
    elseif (i == 24)
        fraf = 0.85;
        fram = 0.7;    
        fbwm = 100;
        fbwf = 100;  
    elseif (i == 31)
        fbwm = 2500;
    elseif (i == 33) || (i == 34)|| (i == 42)
        fraf = 0.95;
        fram = 0.85; 
    end
    mov = readimage(moving0,i);
    fix = readimage(fixed0,i);
    movg = rgb2gray(mov);
    movg = imresize(movg,[224,224]);
    level1 = graythresh(movg);
    bmov = imbinarize(movg,level1*fram);
    
    bmov = bwareaopen(bmov, fbwm);
    %bmov = imbinarize(movg,'adaptive');
    bmov = imfill(bmov,4,'holes');%figure; imshow(bfix)
    bmov = uint8( bmov(:,:,[1 1 1]) * 255 );
    fixg = rgb2gray(fix);
    fixg = imresize(fixg,[224,224]);
    level2 = graythresh(fixg);
    bfix = imbinarize(fixg,level2*fraf);
    bfix = bwareaopen(bfix, fbwf);
    %bfix = imbinarize(fixg,'adaptive');
    bfix = imfill(bfix,4,'holes');%figure; imshow(bfix)
    bfix = uint8( bfix(:,:,[1 1 1]) * 255 );
    figure;
    subplot(2,1,1)
    imshow(bmov)
    subplot(2,1,2)
    imshow(bfix)
    
    [folder, baseFileNameNoExt, ext] = fileparts(fullFileNamesm0{i});
    baseFileNameWithExt = [ baseFileNameNoExt, ext];
    %imwrite(bmov, ['bwfish/',sprintf('%d.jpg',i-1)]);
    imwrite(bmov, ['bwfish/',baseFileNameWithExt]);
    [folder, baseFileNameNoExt, ext] = fileparts(fullFileNamesm0{i});
    baseFileNameWithExt = [ baseFileNameNoExt, ext];
    %imwrite(bfix, ['bwnano/',sprintf('%d.jpg',i-1)]);    
    imwrite(bfix, ['bwnano/',baseFileNameWithExt]);    
end
%}
%load binarize images
sprintf('Start loading image files')
moving = imageDatastore('bwfish','LabelSource','foldernames');
fixed = imageDatastore('bwnano','LabelSource','foldernames');
%moving = moving0;
%fixed = fixed0;

layer1 = 'res3a_relu';
augmoving = augmentedImageDatastore(inputSize(1:2),moving);
augfixed = augmentedImageDatastore(inputSize(1:2),fixed);

featuresmoving = activations(net,augmoving, layer1,'OutputAs','rows');
featuresfixed = activations(net,augfixed,layer1,'OutputAs','rows');

%compute pairewise distance
PDall1 = [];
for i = 1:size(featuresmoving,1)
    %flatten 
    %refeaturesmoving = reshape(featuresmoving(i,:),[7*7,512]);
    %refeaturesfixed = reshape(featuresfixed(i,:),[7*7,512]);    
    refeaturesmoving = reshape(featuresmoving(i,:),[28*28,512]);
    refeaturesfixed = reshape(featuresfixed(i,:),[28*28,512]); 
    refeaturesmoving = (refeaturesmoving - mean(refeaturesmoving, 'all'))./std(refeaturesmoving,0, 'all');
    refeaturesfixed = (refeaturesfixed - mean(refeaturesfixed, 'all'))./std(refeaturesfixed, 0, 'all');

    PD1 = pdist2(refeaturesmoving, refeaturesfixed);
    PDall1(i,:,:) = PD1;
    
end
%clear {}
layer2 = 'res3b1_relu';
augmoving = augmentedImageDatastore(inputSize(1:2),moving);
augfixed = augmentedImageDatastore(inputSize(1:2),fixed);

featuresmoving = activations(net,augmoving, layer2,'OutputAs','rows');
featuresfixed = activations(net,augfixed,layer2,'OutputAs','rows');

%compute pairewise distance
PDall2 = [];
for i = 1:size(featuresmoving,1)
    %flatten 
    %refeaturesmoving = reshape(featuresmoving(i,:),[7*7,512]);
    %refeaturesfixed = reshape(featuresfixed(i,:),[7*7,512]);    
    refeaturesmoving = reshape(featuresmoving(i,:),[28*28,512]);
    refeaturesfixed = reshape(featuresfixed(i,:),[28*28,512]); 
    refeaturesmoving = (refeaturesmoving - mean(refeaturesmoving, 'all'))./std(refeaturesmoving,0, 'all');
    refeaturesfixed = (refeaturesfixed - mean(refeaturesfixed, 'all'))./std(refeaturesfixed, 0, 'all');

    PD2 = pdist2(refeaturesmoving, refeaturesfixed);
    PDall2(i,:,:) = PD2;
    
end

layer3 = 'res3b2_relu';
augmoving = augmentedImageDatastore(inputSize(1:2),moving);
augfixed = augmentedImageDatastore(inputSize(1:2),fixed);

featuresmoving = activations(net,augmoving, layer3,'OutputAs','rows');
featuresfixed = activations(net,augfixed,layer3,'OutputAs','rows');

%compute pairewise distance
PDall3 = [];
for i = 1:size(featuresmoving,1)
    %flatten 
    %refeaturesmoving = reshape(featuresmoving(i,:),[7*7,512]);
    %refeaturesfixed = reshape(featuresfixed(i,:),[7*7,512]);    
    refeaturesmoving = reshape(featuresmoving(i,:),[28*28,512]);
    refeaturesfixed = reshape(featuresfixed(i,:),[28*28,512]); 
    refeaturesmoving = (refeaturesmoving - mean(refeaturesmoving, 'all'))./std(refeaturesmoving,0, 'all');
    refeaturesfixed = (refeaturesfixed - mean(refeaturesfixed, 'all'))./std(refeaturesfixed, 0, 'all');

    PD3 = pdist2(refeaturesmoving, refeaturesfixed);
    PDall3(i,:,:) = PD3;
    
end

layer4 = 'res3b3_relu';
augmoving = augmentedImageDatastore(inputSize(1:2),moving);
augfixed = augmentedImageDatastore(inputSize(1:2),fixed);

featuresmoving = activations(net,augmoving, layer4,'OutputAs','rows');
featuresfixed = activations(net,augfixed,layer4,'OutputAs','rows');

%compute pairewise distance
PDall4 = [];
for i = 1:size(featuresmoving,1)
    %flatten 
    %refeaturesmoving = reshape(featuresmoving(i,:),[7*7,512]);
    %refeaturesfixed = reshape(featuresfixed(i,:),[7*7,512]);    
    refeaturesmoving = reshape(featuresmoving(i,:),[28*28,512]);
    refeaturesfixed = reshape(featuresfixed(i,:),[28*28,512]); 
    refeaturesmoving = (refeaturesmoving - mean(refeaturesmoving, 'all'))./std(refeaturesmoving,0, 'all');
    refeaturesfixed = (refeaturesfixed - mean(refeaturesfixed, 'all'))./std(refeaturesfixed, 0, 'all');

    PD4 = pdist2(refeaturesmoving, refeaturesfixed);
    PDall4(i,:,:) = PD4;
    
end
% use feature points @layer2
layer5 = 'res4a_relu';

featuresmoving = activations(net,augmoving, layer5,'OutputAs','rows');
featuresfixed = activations(net,augfixed,layer5,'OutputAs','rows');
%compute pairewise distance
PDall5 = [];
for i = 1:size(featuresmoving,1)
    %flatten 
    %refeaturesmoving = reshape(featuresmoving(i,:),[7*7,512]);
    %refeaturesfixed = reshape(featuresfixed(i,:),[7*7,512]);    
    refeaturesmoving = reshape(featuresmoving(i,:),[14*14,1024]);
    refeaturesfixed = reshape(featuresfixed(i,:),[14*14,1024]); 
    refeaturesmoving = (refeaturesmoving - mean(refeaturesmoving, 'all'))./std(refeaturesmoving,0,  'all');
    refeaturesfixed = (refeaturesfixed - mean(refeaturesfixed, 'all'))./std(refeaturesfixed,0,  'all');

    PD5 = pdist2(refeaturesmoving, refeaturesfixed);
    PDall5(i,:,:) = PD5;
    
end


layer6 = 'res4b1_relu';

featuresmoving = activations(net,augmoving, layer6,'OutputAs','rows');
featuresfixed = activations(net,augfixed,layer6,'OutputAs','rows');
%compute pairewise distance
PDall6 = [];
for i = 1:size(featuresmoving,1)
    %flatten 
    %refeaturesmoving = reshape(featuresmoving(i,:),[7*7,512]);
    %refeaturesfixed = reshape(featuresfixed(i,:),[7*7,512]);    
    refeaturesmoving = reshape(featuresmoving(i,:),[14*14,1024]);
    refeaturesfixed = reshape(featuresfixed(i,:),[14*14,1024]); 
    refeaturesmoving = (refeaturesmoving - mean(refeaturesmoving, 'all'))./std(refeaturesmoving,0,  'all');
    refeaturesfixed = (refeaturesfixed - mean(refeaturesfixed, 'all'))./std(refeaturesfixed,0,  'all');

    PD6 = pdist2(refeaturesmoving, refeaturesfixed);
    PDall6(i,:,:) = PD6;
    
end

% use feature points @layer5,6
layer7 = 'res5a_relu';

featuresmoving = activations(net,augmoving, layer7,'OutputAs','rows');
featuresfixed = activations(net,augfixed,layer7,'OutputAs','rows');
%compute pairewise distance
PDall7 = [];
for i = 1:size(featuresmoving,1)
    %flatten 
    refeaturesmoving = reshape(featuresmoving(i,:),[7*7,2048]);
    refeaturesfixed = reshape(featuresfixed(i,:),[7*7,2048]);    
    %refeaturesmoving = reshape(featuresmoving(i,:),[28*28,128]);
    %refeaturesfixed = reshape(featuresfixed(i,:),[28*28,128]); 
    refeaturesmoving = (refeaturesmoving - mean(refeaturesmoving, 'all'))./std(refeaturesmoving,0,  'all');
    refeaturesfixed = (refeaturesfixed - mean(refeaturesfixed, 'all'))./std(refeaturesfixed, 0, 'all');

    PD7 = pdist2(refeaturesmoving, refeaturesfixed);
    PDall7(i,:,:) = PD7;
end

layer8 = 'res5b_relu';
featuresmoving = activations(net,augmoving, layer8,'OutputAs','rows');
featuresfixed = activations(net,augfixed,layer8,'OutputAs','rows');
%compute pairewise distance
PDall8 = [];
for i = 1:size(featuresmoving,1)
    %flatten 
    refeaturesmoving = reshape(featuresmoving(i,:),[7*7,2048]);
    refeaturesfixed = reshape(featuresfixed(i,:),[7*7,2048]);    
    %refeaturesmoving = reshape(featuresmoving(i,:),[28*28,128]);
    %refeaturesfixed = reshape(featuresfixed(i,:),[28*28,128]); 
    refeaturesmoving = (refeaturesmoving - mean(refeaturesmoving, 'all'))./std(refeaturesmoving,0,  'all');
    refeaturesfixed = (refeaturesfixed - mean(refeaturesfixed, 'all'))./std(refeaturesfixed, 0, 'all');

    PD8 = pdist2(refeaturesmoving, refeaturesfixed);
    PDall8(i,:,:) = PD8;
end
layer9 = 'res5c_relu';
featuresmoving = activations(net,augmoving, layer9,'OutputAs','rows');
featuresfixed = activations(net,augfixed,layer9,'OutputAs','rows');
%compute pairewise distance
PDall9 = [];
for i = 1:size(featuresmoving,1)
    %flatten 
    refeaturesmoving = reshape(featuresmoving(i,:),[7*7,2048]);
    refeaturesfixed = reshape(featuresfixed(i,:),[7*7,2048]);    
    %refeaturesmoving = reshape(featuresmoving(i,:),[28*28,128]);
    %refeaturesfixed = reshape(featuresfixed(i,:),[28*28,128]); 
    refeaturesmoving = (refeaturesmoving - mean(refeaturesmoving, 'all'))./std(refeaturesmoving,0,  'all');
    refeaturesfixed = (refeaturesfixed - mean(refeaturesfixed, 'all'))./std(refeaturesfixed, 0, 'all');

    PD9 = pdist2(refeaturesmoving, refeaturesfixed);
    PDall9(i,:,:) = PD9;
end
siz1 = size(PD1);
siz2 = size(PD2);
siz3 = size(PD3);
siz4 = size(PD4);
siz5 = size(PD5);
siz6 = size(PD6);
siz7 = size(PD7);
siz8 = size(PD8);
siz9 = size(PD9);
%PD = 1*PD1 + expand_dim(PD2,2) + expand_dim(PD3,4);
%imid = 1;sigma = 500;
sigma = 500;
thf = 0.25;
thf1 = 0.5;
thf2 = 0.15;
for imid = [7,20,28]%1:42% 14:42
    close all;
PD = 2*(reshape(PDall1(imid,:,:),siz1)+reshape(PDall2(imid,:,:),siz2) +...
    reshape(PDall3(imid,:,:),siz3)+reshape(PDall4(imid,:,:),siz4))...
    + 1.14*(expand_dim(reshape(PDall5(imid,:,:),siz5),2)+expand_dim(reshape(PDall6(imid,:,:),siz6),2))...
    + expand_dim(reshape(PDall7(imid,:,:),siz7),4)+expand_dim(reshape(PDall8(imid,:,:),siz8),4)+expand_dim(reshape(PDall9(imid,:,:),siz9),4);
%PD = 1*reshape(PDall1(imid,:,:),siz1);
% PD = PD1;
if imid == 19
    sigma = 5;thf = 0.2;thf1 = 0.25;thf2 = 0.05;
end
if imid == 20
    sigma = 500;thf = 0.2;thf1 = 0.75;thf2 = 0.25;
end
if imid == 25
    sigma = 250;thf = 0.3;thf1 = 0.75;thf2 = 0.25;
end
if imid == 29
    sigma = 100;thf = 0.2;thf1 = 0.25;thf2 = 0.05;
end
if imid == 27
    sigma = 1200;thf = 0.2;thf1 = 0.6;thf2 = 0;
end
if imid == 40
    sigma = 100;thf = 0.2;thf1 = 0.25;thf2 = 0.05;
end
if imid == 7
    sigma = 2000;thf = 0.2;thf1 = 0.15;thf2 = 0.01;
end
if imid == 6
    thf = 0.25;
end
if (imid == 5) 
    sigma = 1500;
end
if (imid == 9)
    sigma = 1000;thf1 = 0.4;thf2 = 0.1;
end
if (imid == 28)
    sigma = 2000;thf = 0.2;thf1 = 0.5;thf2 = 0.25;
end
%PD = 1*reshape(PDall1(imid,:,:),siz1);
% PD = PD1;
%match points
sprintf('Initiating first round points matching')
px = [1:1:size(PD,1)];%initiate matching matrix row seq
[mpd, mid] = min(PD,[],2);%return the mim of each row
nc = [px',mid]; %initiate matching points
ind = sub2ind(size(PD),px',mid);
mc1 = PD(ind);
mk = ones(size(PD,1));%create mask with all ones
mk(ind) = NaN; %assign NaN values
mkd = PD.*mk;%mask distance matrix 
mc2 = min(mkd,[],2);%return the mim of each row
quality = mc2./mc1; %quality of matching
maxq = max(quality);
cond = quality >= maxq;

while (size(quality(quality >= maxq),1)) <= size(PD,1)*0.2
    % size(quality(quality >= maxq),1), maxq
    maxq = maxq - 0.001;
end

indc = find(quality >= maxq);
nnc = nc(indc,:);    
cnt = size(nnc,1);

seq = [repelem(1:28,28);repmat(1:28,1,28)]';
%seq = [repelem(1:7,7);repmat(1:7,1,7)]';

rc1 = reshape(nnc(:,2),[cnt,1]);
rc2 = reshape(nnc(:,1),[1,cnt]);
nrc1 = repmat(rc1,1,cnt);
nrc2 = repmat(rc2,cnt,1);
PDx = [];
for i = 1:size(nrc1,1)
    for j = 1:size(nrc1,2)
        PDx(i,j) = PD(nrc1(i,j),nrc2(i,j));
    end
end


clear {px,mpd,mid,nc,ind,mc1,mk,mkd,mc2,quality,maxq,cond,minq}

%match points again with PDx

sprintf('Initiating second round points matching')
px = [1:1:size(PDx,1)];%initiate matching matrix row seq
[mpd, mid] = min(PDx,[],2);
nc = [px',mid]; 
ind = sub2ind(size(PDx),px',mid);
mc1 = PDx(ind);
mk = ones(size(PDx,1));%create mask with all ones
mk(ind) = NaN; %assign NaN values
mkd = PDx.*mk;%mask distance matrix 
mc2 = min(mkd,[],2);%return the mim of each row
quality = mc2./mc1;
maxq = max(quality);
cond = quality >= maxq;
minq = min(quality);

while (size(quality(quality >= maxq),1) <= cnt*0.1)
    maxq = maxq - 0.001;
end

fq = maxq;
delta = (maxq - minq)/10;

%seq = [repelem(1:7,7);repmat(1:7,1,7)]';
X = seq*8-4;
Y = seq*8-4;
X = (X-112)./224;
Y = (Y-112)./224;
X = X(nnc(:,2),:);

%X = sort(X,1);
Y = Y(nnc(:,1),:);

M = size(X,1);
N = size(Y,1);

% get shape context
nbins_theta = 12;
nbins_r = 5;
r_inner = 0.1;
r_outer = 2.5;%cost_scz
nbins = nbins_theta*nbins_r;
sc = ShapeContext(X,nbins_theta,nbins_r,r_inner,r_outer);

% prepare for registration
sprintf('Initiating optimization')

while (itr < max_itr) && (abs(dQ) > tau) && (isigma > 1e-4)
   Zo = Z;
   Qo = Q;
   Lt = [];
   L = [];
   
   if mod((itr - 1),freq) == 0
       indnc = find(quality >= fq);
       nc2 = nc(indnc,:);
       for i = 1:size(nc2,1)
           Lt(i) = PDx(nc2(i,1),nc2(i,2));
       end
       if max(Lt)>0
           Lt = Lt/max(Lt);
       end
       L = ones(M,N);
       for i = 1:size(nc2,1)
           L(nc2(i,1),nc2(i,2)) = Lt(i);
       end
       Lo = L;
       scz = ShapeContext(Z,nbins_theta,nbins_r,r_inner,r_outer);
       cost_scz = cost_sc(scz,sc, nbins);
       L = L.*cost_scz;
       [nnc2,] = lapjv(L);%solve linear assignment
       Pr = ones(size(PDx)).*(1-epsilon)/N;
       for i = 1:size(nnc2,2)
            Pr(i,nnc2(i)) = 1;
       end
       %Pr(logical(eye(size(Pr)))) = diag(1);
       
       Pr = Pr./sum(Pr);
       fq = fq - delta;
       if fq < minq
           fq = minq;
       end
   
   end
  
   [P0, P1, Np, tmp, Q] = mncompute(X,Y,Zo,Pr,isigma, omega);
   Q = Q + lamda/2*trace((Ax'*grb)*Ax);%dot product
   dP = diag(P1);
   t1 = dP*grb + lamda*isigma*eye(M);%dot product
   t2 = P0*X - dP*Y;%dot product
   Ax = t1\t2;%dot product, inv(t1) is bad
   isigma = tmp/(2*Np);
   omega = min(max(1,1 - Np/N),0.01);
   Z = Y + grb*Ax;%dot product
   lamda = min(0.1, lamda*0.95);
   dQ = Q - Qo;
   
   
   itr = itr + 1;
   
end

sprintf('Optimization completed, iteration taken %d',itr)

Xn = (X*224+112);
Yn = (Y*224+112);
Zn = (Z*224+112);


Xo = Xn;
Yo = Yn;
Zo = Zn;

[Xnn,ind] = unique(Xn,'rows','stable');
Ynn = Yn(ind,:);
Znn = Zn(ind,:);
Xn = Xnn;
Yn = Ynn;
Zn = Znn;
%X1 = sqrt(Xn(:,1).^2 + Xn(:,2).^2);
%Y1 = sqrt(Yn(:,1).^2 + Yn(:,2).^2);
%Z1 = sqrt(Zn(:,1).^2 + Zn(:,2).^2);
dX1 = Xn - Yn;
dY1 = sqrt(dX1(:,1).^2 + dX1(:,2).^2);
Xs = [];
dY2 = dY1;
dY2(dY2 == 0) = [];

for i = 1: size(dY1,1)
     
    if  (imid == 5) || (imid == 40) || (imid == 25) || (imid == 7) %|| (imid == 28) 
        if ((dY1(i) > quantile(dY1,thf))) %|| (dY1(i) == 0)
            Xs = [Xs;i];
        end
    else
        if ((dY1(i) > quantile(dY2,thf1)) || (dY1(i) < quantile(dY2,thf2))) %|| (dY1(i) == 0)
            Xs = [Xs;i];
        end           
    end
    
end

Xn(Xs,:) = [];
Yn(Xs,:) = [];
Zn(Xs,:) = [];
    
%{
while (~issorted(Xn(:,1)))
    Xs = [];
    m0 = size(Xn,1);
    for i = 1: m0-1
        if (Xn(i,1) >  Xn(i+1,1)) %|| (Xn(i,1) >  Xn(i+1,1))
            Xs = [Xs;i];
        end
    end
    Xn(Xs,:) = [];
    Yn(Xs,:) = [];
    Zn(Xs,:) = [];
end

while (~issorted(Yn(:,1)))
    Xs = [];
    m0 = size(Yn,1);
    for i = 2: m0-1
        if (Yn(i,1) <  Yn(i-1,1)) || (Yn(i,1) >  Yn(i+1,1))
            Xs = [Xs;i];
        end
    end
    Xn(Xs,:) = [];
    Yn(Xs,:) = [];
    Zn(Xs,:) = [];
end
%}
%% Thin plate warping
sprintf('Start interplation and warping')
mov = readimage(moving0,imid);
%movg = rgb2gray(mov);
movr = imresize(mov, [224,224]);

movu = readimage(movingu,imid);
%movu = rgb2gray(movu);
movu = imresize(movu, [224,224]);

mov0 = readimage(moving,imid);
mov0 = rgb2gray(mov0);
mov0 = imresize(mov0, [224,224]);

fix0 = readimage(fixed,imid);
fix0 = rgb2gray(fix0);
fix0 = imresize(fix0, [224,224]);

fix = readimage(fixed0,imid);
fixr = imresize(fix, [224,224]);

%inliner selection;
[F,inliersIndex] = estimateFundamentalMatrix(Xn,Yn);

sprintf('Matching inliner percentage %d',size(Yn(inliersIndex,:),1)/size(Yn,1)*100)


[rmovc,maskc] = rbfwarp2d(movr, Yn(inliersIndex,:),Zn(inliersIndex,:),'thin');
figure;imshow(uint8(rmovc))
t = fitgeotrans(Yn(inliersIndex,:),Xn(inliersIndex,:),'similarity');
registered = imwarp(movr,t,'OutputView',imref2d(size(fixr)));
fg = figure;imshow(registered, 'border', 'tight')
set(fg,'units','pixels'); % set the axes units to pixels
x = get(fg,'position'); % get the position of the axes
set(fg,'units','pixels'); % set the figure units to pixels
y = get(fg,'position'); % get the figure position
set(fg,'position',[y(1) y(2) x(3) x(4)]);% set the position of the figure to the length and width of the axes
set(fg,'units','normalized','position',[0 0 1 1]); % set the axes units to pixels
filename = sprintf('Registered FISH warp %d.png',imid-1);
print('-dtiff','-r300',filename)

fg = figure; hold on
axis tight
subplot(2,4,1)
imshow(movu, 'border', 'tight')
title('FISH');

subplot(2,4,3)
%[rmov1,mask1] = rbfwarp2d(movr, Yn, Zn,'thin');
%[rmov2,mask2] = rbfwarp2d(mov0, Yn, Zn,'thin');

[rmov1,mask1] = rbfwarp2d(movr, Yn(inliersIndex,:),Zn(inliersIndex,:),'thin');
[rmov2,mask2] = rbfwarp2d(mov0, Yn(inliersIndex,:),Zn(inliersIndex,:),'thin');
%[rmov1,mask1] = rbfwarp2d(movr, Yn, Zn,'gau',0.5);
imshow(uint8(rmov1), 'border', 'tight');
title('FISH registered');

subplot(2,4,2)
imshow(fixr, 'border', 'tight');
title('nanoSIMS');

subplot(2,4,7)
imshow(rmov2, 'border', 'tight');
title('FISH registered');

subplot(2,4,4)
%imshowpair(fixr,rmov1,'blend','Scaling','none')
%imshowpair(fixr,rmov1,'blend','Scaling','joint')
imshowpair(rmov1,fixr,'checkerboard');
title('TPS registered check');
subplot(2,4,8)
%imshowpair(fixr,rmov1,'blend','Scaling','none')
%imshowpair(fixr,rmov1,'blend','Scaling','joint')
imshowpair(rmov2,fix0,'checkerboard');
title('TPS registered check');
subplot(2,4,5)
imshow(mov0, 'border', 'tight');
title('FISH bw');
subplot(2,4,6)
imshow(fix0, 'border', 'tight')
title('nanoSIMS bw');
set(fg,'units','pixels'); % set the axes units to pixels
x = get(fg,'position'); % get the position of the axes
set(fg,'units','pixels'); % set the figure units to pixels
y = get(fg,'position'); % get the figure position
set(fg,'position',[y(1) y(2) x(3) x(4)]);% set the position of the figure to the length and width of the axes
set(fg,'units','normalized','position',[0 0 1 1]); % set the axes units to pixels
filename = sprintf('Registered FISH all %d.png',imid-1);
print('-dtiff','-r300',filename)


figure;
imshow(uint8(rmov1), 'border', 'tight');
set(gca,'units','pixels'); % set the axes units to pixels
x = get(gca,'position'); % get the position of the axes
set(gcf,'units','pixels'); % set the figure units to pixels
y = get(gcf,'position'); % get the figure position
set(gcf,'position',[y(1) y(2) x(3) x(4)]);% set the position of the figure to the length and width of the axes
set(gca,'units','normalized','position',[0 0 1 1]); % set the axes units to pixels

filename = sprintf('Registered FISH %d.png',imid-1);
print('-dtiff','-r300',filename)

figure;
imshowpair(rmov1,fixr,'checkerboard');
set(gca,'units','pixels'); % set the axes units to pixels
x = get(gca,'position'); % get the position of the axes
set(gcf,'units','pixels'); % set the figure units to pixels
y = get(gcf,'position'); % get the figure position
set(gcf,'position',[y(1) y(2) x(3) x(4)]);% set the position of the figure to the length and width of the axes
set(gca,'units','normalized','position',[0 0 1 1]); % set the axes units to pixels

filename = sprintf('Registered FISH check %d.png',imid-1);
print('-dtiff','-r300',filename)


%subplot(3,3,8)
%imshow(uint8(rmov1), 'border', 'tight');hold on;
%plot( Yn(:,1),Yn(:,2),'r.' );

%title('Matached features');
%subplot(3,3,9)
%imshow(fixr, 'border', 'tight');hold on;
%plot( Zn(:,1),Zn(:,2),'g.' );
%title('Transformed features');
%set(gca,'XColor','none','YColor','none');

figure;showMatchedFeatures(movr,fixr,Yn(inliersIndex,:),Xn(inliersIndex,:),'montage','PlotOptions',{'ro','go','b-'});
set(gca,'units','pixels'); % set the axes units to pixels
x = get(gca,'position'); % get the position of the axes
set(gcf,'units','pixels'); % set the figure units to pixels
y = get(gcf,'position'); % get the figure position
set(gcf,'position',[y(1) y(2) x(3) x(4)]);% set the position of the figure to the length and width of the axes
set(gca,'units','normalized','position',[0 0 1 1]); % set the axes units to pixels
filename = sprintf('Registered Matching points inlier %d.png',imid-1);
print('-dtiff','-r300',filename)

figure;showMatchedFeatures(movr,fixr,Yo,Xo,'montage','PlotOptions',{'ro','go','b-'});
set(gca,'units','pixels'); % set the axes units to pixels
x = get(gca,'position'); % get the position of the axes
set(gcf,'units','pixels'); % set the figure units to pixels
y = get(gcf,'position'); % get the figure position
set(gcf,'position',[y(1) y(2) x(3) x(4)]);% set the position of the figure to the length and width of the axes
set(gca,'units','normalized','position',[0 0 1 1]); % set the axes units to pixels
filename = sprintf('Registered Matching points before %d.png',imid-1);
print('-dtiff','-r300',filename)

figure;showMatchedFeatures(movr,fixr,Yn,Xn,'montage','Parent',axes);
set(gca,'units','pixels'); % set the axes units to pixels
x = get(gca,'position'); % get the position of the axes
set(gcf,'units','pixels'); % set the figure units to pixels
y = get(gcf,'position'); % get the figure position
set(gcf,'position',[y(1) y(2) x(3) x(4)]);% set the position of the figure to the length and width of the axes
set(gca,'units','normalized','position',[0 0 1 1]); % set the axes units to pixels

filename = sprintf('Registered Matching points after %d.png',imid-1);
print('-dtiff','-r300',filename)


%y1 = RBFinterp2(Yn(:,1),Yn(:,2),Zn(:,1), 'TPS2' , 2.5);
%y2 = RBFinterp2(Yn(:,1),Yn(:,2),Zn(:,2), 'TPS2' , 2.5);
%figure; imshow(y)


%% Visualize current Convolutional Layers
%{
channels = 1:4;
%layer = 32;
%name = net.Layers(layer).Name;

for i = 1:3
    if i ==1
        layer = layer1;
    elseif i ==2
        layer = layer2; 
    elseif i ==3
        layer = layer3;        
    elseif i ==4
        layer = layer4; 
    elseif i ==5
        layer = layer5; 
    elseif i ==6
        layer = layer6;       
    elseif i ==7
        layer = layer7;       
    elseif i ==8
        layer = layer8;       
    elseif i ==9
        layer = layer9;         
    end
I1 = deepDreamImage(net,layer,channels, ...
    'Verbose',false, ...
    'PyramidLevels',1);
figure;
I1s = imtile(I1,'Frames', 1:4, 'GridSize', [1 4]);
%I2 = imtile(I2,'ThumbnailSize',[64 64]);
%I3 = imtile(I3,'ThumbnailSize',[64 64]);
imshow(I1s)
set(gca,'units','pixels'); % set the axes units to pixels
x = get(gca,'position'); % get the position of the axes
set(gcf,'units','pixels'); % set the figure units to pixels
y = get(gcf,'position'); % get the figure position
set(gcf,'position',[y(1) y(2) x(3) x(4)]);% set the position of the figure to the length and width of the axes
set(gca,'units','normalized','position',[0 0 1 1]); % set the axes units to pixels
filename = sprintf('Registered Extracted Feature %d layer%d.png',[imid-1,i]);
print('-dtiff','-r300',filename)
end
%}
end
%imshow(I2)
%imshow(I3)
%name = net.Layers(layer).Name;
%title(['Layer ',name,' Features'],'Interpreter','none')

%}

 